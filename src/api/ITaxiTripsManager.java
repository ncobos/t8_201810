package api;

import model.data_structures.Lista;
import model.vo.Service;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

/**
 * API para la clase de logica principal  
 */

public interface ITaxiTripsManager 
{
	
	//1C
	/**
	 * Dada la direccion del json que se desea cargar, se generan vo's, estructuras y datos necesarias
	 * @param direccionJson, ubicacion del json a cargar
	 * @return true si se lo logro cargar, false de lo contrario
	 */
	public boolean cargarSistema(String direccionJson);
	
	public void crearGrafo(double distancia);
}