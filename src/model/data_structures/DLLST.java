package model.data_structures;

//CLASE QUE DESCRIBE UNA DOUBLE LINKED LIST SYMBOL TABLE
public class DLLST <K extends Comparable<K>, V > 
{
		private HashNode<K, V> primerNodo;
		private HashNode<K, V> actual;
		
		private int size;
		public int posActual;

		/**
		 * Construye una lista doble
		 */
		public DLLST() 
		{
			
			primerNodo = null;
			actual = null;
			
			size = 0;
		}

		/**
		 * Construye una lista doble con un solo nodo
		 */

		public Integer getSize() {
			return size;
		}
		
		public HashNode<K, V> next()
		{
			HashNode<K, V> nodo = actual;
			if(actual != null) {
			actual = actual.getNext();
			}
			return nodo;
		}
		
		public boolean hasNext() 
		{
			if(size != 0 && actual != null)
				return true;
			else
			{
				actual = primerNodo;
				return false;
			}
		}


		public boolean existElement(K llave)
		{
			boolean existe = false;
			if(size != 0)
			{
				HashNode<K, V> temp = primerNodo;
				int i = 0;
				while(i < size && !existe)
				{
					if(temp.darLlave().compareTo(llave) == 0)
					{
						existe = true;
					}
					temp = temp.getNext();
					i++;
				}
			}

			return existe;
		}

		public V findElement(K llave) 
		{
			V existe = null;
			boolean t = false;
			if(size != 0)
			{
				HashNode<K, V> temp = primerNodo;
				int i = 0;
				while(i < size && !t )
				{
					if(temp.darLlave().compareTo(llave) == 0)
					{
						existe = temp.getValue();
						t = true;
					}
					temp = temp.getNext();
					i++;
				}
			}

			return existe;
		}
		
		public void putElement(K llave, V valor) 
		{
			V existe = null;
			boolean t = false;
			
			if(size != 0)
			{
				HashNode<K, V> temp = primerNodo;
				int i = 0;
				while(i < size && !t )
				{
					if(temp.darLlave().compareTo(llave) == 0)
					{
						temp.setValue(valor);
						
						t = true;
					}
					temp = temp.getNext();
					i++;
				}
			}
}



		/**
		 * Devuelve el elemento del nodo que se encuentra en la posicion K
		 * @param k la posici�n del nodo del que se desea el elemento.
		 * @return el elemento  posici�n
		 * @throws Exception si pos < 0 o pos >= size() 
		 * @throws Exception si la lista esta vacia
		 */

		
		public boolean add(V elementToAdd, K key) throws Exception
		{
			boolean add = false;
			HashNode<K, V> nodo = new HashNode(key, elementToAdd);
			if (elementToAdd == null)
			{
				throw new NullPointerException("el elemento es nulo");		
			}
			if(size == 0)
			{	
				primerNodo = nodo;
				actual = primerNodo;
			
				add = true;
				size++;
			}
			else
			{
				nodo.setNext(primerNodo);
				primerNodo.setAnterior(nodo);
				
				size++;
				posActual = 1;
				primerNodo = nodo;
				actual = primerNodo;

				add = true;
			}

			actual = primerNodo;
			return add;
		}

		/**
		 * Elimina el nodo actual
		 * @return true si el elemento fue eliminado
		 * @return false si la lista esta vacia
		 */
		public boolean delete(K llave) 
		{
			boolean delete = false;
			HashNode<K, V> temp = primerNodo;
			if(getSize() == 0)
			{
				delete = false;
			}
			else if(primerNodo.darLlave().compareTo(llave) == 0 && size == 1)
			{
				primerNodo = null;
				actual = primerNodo;
				size--;
				delete  = true;
			}
			else
			{ 
				int i = 0;
				while(i < size  && !delete) 
				{
					if(llave.compareTo(temp.darLlave()) == 0)
					{
						HashNode<K, V> temp1 = temp;
						if(primerNodo == temp)
						{
							primerNodo = primerNodo.getNext();
							primerNodo.setAnterior(null);
							actual = primerNodo;
						}
						else 
						{
						temp.getAnterior().setNext(temp.getNext());
						if(temp1.getNext() != null)
						temp.getNext().setAnterior(temp1.getAnterior());;
						}

						delete = true;
						size--;
					}
					else
					{
						i++;
						temp = temp.getNext();
					}
				}
			}

			return delete;
		}
	}

	
