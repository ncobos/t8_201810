package model.vo;

import model.data_structures.LinkedList;

public class ServiciosValorPagado {
	
	private LinkedList<Service> serviciosAsociados;
	private double valorAcumulado;
	
	public ServiciosValorPagado (LinkedList s, double v ){
		serviciosAsociados = s ;
		valorAcumulado = v ;
	}
	public LinkedList<Service> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(LinkedList<Service> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado() {
		return valorAcumulado;
	}
	public void setValorAcumulado(double valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}
	
	

}
