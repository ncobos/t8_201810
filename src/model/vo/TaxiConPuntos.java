package model.vo;

public class TaxiConPuntos extends Taxi {

	double puntos;
	public TaxiConPuntos(String pId, String pCom, double pPuntos) {
		super(pId, pCom);
		puntos = pPuntos;
		// TODO Auto-generated constructor stub
	}

	/**
     * @return puntos - puntos de un Taxi
     */
	public double getPuntos(){
		return puntos;
	}
}
