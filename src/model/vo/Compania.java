package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private Lista<Taxi> taxisInscritos;	
	
	public Compania ( String name, Taxi t)
	{
		nombre = name ;
		taxisInscritos = new Lista<Taxi>();
		taxisInscritos.add(t);
	}
	public Compania ( String name,Lista<Taxi> t)
	{
		nombre = name ;
		taxisInscritos =  t;
		
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedList<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(Taxi ele) {
		this.taxisInscritos.add(ele);
	}

	@Override
	public int compareTo(Compania o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	

}
