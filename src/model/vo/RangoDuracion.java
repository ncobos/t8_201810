package model.vo;

import model.data_structures.Lista;

/**
 * Modela una rango de fechas y horas (iniciales y finales)
 *
 */
public class RangoDuracion implements Comparable<RangoDuracion>
{
	//ATRIBUTOS
	
    /**
     * Modela el tiempo inicial
     */
	private String tiempoInicial; 

    /**
     * Modela el tiempo final
     */
	private String tiempoFinal; 
	
	/**
	 * Lista de servicios
	 */
	private Lista<Service> listaServicios;
	
	//CONSTRUCTOR
	/**
	 * @param pFechaInicial, fecha inicial del rango
	 * @param pFechaFinal, fecha final del rango
	 * @param pHoraInicio, hora inicial del rango
	 * @param pHoraFinal, hora final del rango
	 */
	public RangoDuracion(String inicial, String pFinal)
	{
		this.tiempoInicial = inicial;
		this.tiempoFinal = pFinal;
		listaServicios = new Lista<Service>();
		
	}
	//M�TODOS
	
	/**
	 * @return the tiempoInicial
	 */
	public String getInicio() 
	{
		return tiempoInicial;
	}

	/**
	 * @return the tiempoInicial
	 */
	public String getFinal() 
	{
		return tiempoFinal;
	}
	
	public Lista<Service> getLista()
	{
		return listaServicios;
	}
	
	public Boolean estaDentro (String pDuracion)
	{
		Boolean rta = false;
		if(Integer.parseInt(tiempoInicial)<= Integer.parseInt(pDuracion) &&  Integer.parseInt(tiempoFinal)>=  Integer.parseInt(pDuracion))
		{rta = true;}
		return rta;
	}
	@Override
	public int compareTo(RangoDuracion o) {
		// TODO Auto-generated method stub
		return tiempoInicial.compareToIgnoreCase(o.getInicio());
	}
	
	public void agregarServicio(Service pServicio)
	{
		listaServicios.add(pServicio);
	}
}
