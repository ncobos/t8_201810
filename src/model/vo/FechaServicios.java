package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class FechaServicios implements Comparable<FechaServicios>{
	
	private String fecha;
	private Lista<Service> serviciosAsociados;
	private int numServicios;
	private double horaInicio;
	private double horaFin;
	
	public FechaServicios(int pHoraInicio)
	{
		horaInicio = pHoraInicio*100000;
		horaFin = horaInicio + 1500000;
		serviciosAsociados = new Lista<Service>();
	}
	public FechaServicios( String f )
	{
		fecha =f ;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Lista<Service> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(Lista<Service> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public int getNumServicios() {
		return numServicios;
	}
	public void setNumServicios(int numServicios) {
		this.numServicios = numServicios;
	}
	public double getHoraInicio()
	{
		return horaInicio;
	}
	public void agregarServicio(Service pServicio)
	{
		serviciosAsociados.add(pServicio);
	}
	@Override
	public int compareTo(FechaServicios o) {
		// TODO Auto-generated method stub
		return fecha.compareToIgnoreCase(o.fecha);
	}
	
	public Boolean estaDentro(double pHora)
	{
		Boolean rta = false;
		if(pHora >= horaInicio && pHora <=horaFin)
		{
			rta = true;
		}
		return rta;
	}
	
	

}
