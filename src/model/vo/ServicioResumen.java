package model.vo;

public class ServicioResumen implements Comparable<ServicioResumen>{

	
	private int numOfServices;
	private double tripMiles;
	private double tripTotal;
	
	private String taxiId;
	
	
	public ServicioResumen (int pNum, double pMil, double pTot, String pTaxiId)
	{
		numOfServices = pNum;
		tripMiles = pMil;
		tripTotal = pTot;
		taxiId = pTaxiId;
	}
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getNumOfServices() {
		// TODO Auto-generated method stub
		return numOfServices;
	}
	
	public String getId()
	{
		return taxiId;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotalCost() {
		// TODO Auto-generated method stub
		return tripTotal;
	}


	@Override
	public int compareTo(ServicioResumen o) {
		// TODO Auto-generated method stub
		if(o == null)
		{
			System.out.println("el objeto o es null");
		}
		return taxiId.compareToIgnoreCase(o.getId());
	}
	
}