package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {


	private String tripId;

	private int tripSeconds;

	private double tripMiles;

	private double tripTotal;

	private String tripStart;

	private String tripEnd;

	private int dropoffCommunityArea;

	private int pickupCommunityArea;

	private int dropoffCensusTract;

	private int dropoffCentroidLongitude;

	private double latitudInicio;
	
	private double longitudInicio;
	Taxi taxi = null;


	public Service (String pTripId, int pSec, double pMil, double pTot, int pComArea,int pUp, String taxiId, String pCom, String pStart, String pEnd,double pLatitudInicio, double pLongitudinicio)
	{
		tripId = pTripId;

		tripSeconds = pSec;

		tripMiles = pMil;

		tripTotal = pTot;

		dropoffCommunityArea = pComArea;

		pickupCommunityArea = pUp;

		tripStart = pStart;

		tripEnd = pEnd;
		
		latitudInicio= pLatitudInicio;
		
		longitudInicio = pLongitudinicio;

		taxi = new Taxi(taxiId, pCom);
	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	

	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi.getTaxiId();
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}

	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotalCost() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return tripStart.compareToIgnoreCase(o.tripStart);
	}

	public int compareStart(String pStart)
	{
		int rta = tripStart.compareToIgnoreCase(pStart);

		return rta;
	}

	public int compareEnd(String pEnd)
	{
		int rta = tripEnd.compareToIgnoreCase(pEnd);

		return rta;
	}

	public int getDropComArea()
	{
		return dropoffCommunityArea;
	}

	public int getPickupCommunityArea() {
		return pickupCommunityArea;
	}

	public Taxi getTaxi()
	{
		return taxi;
	}

	public String getTripEnd()
	{
		return tripEnd;
	}

	public String getTripStart()
	{
		return tripStart;
	}

	public int getDropOffZone(){
		//TODO Auto-generated method stub
		return dropoffCommunityArea;
	}


	public double getPickupLatitud(){
		//TODO Auto-generated method stub
		return latitudInicio;
	}

	public double getPickupLongitud(){
		//TODO Auto-generated method stub
		return longitudInicio;
	}

	public String getCompany()
	{
		return taxi.getCompany();
	}
	
	public String getStartHour()
	{
		return getTripStart().substring(11);
	}
	
	public String getFinishHour()
	{
		return getTripEnd().substring(11);
	}
	
	public double getTimeFromStart()
	{
		double rta;
		String remplazo = getStartHour().replace(":", "");
		String remplazofinal = remplazo.replace(".", "");
		rta = Double.parseDouble(remplazofinal);
//		double resta = 90000000;
//		rta -= 20000000;
		return Math.abs(rta);
	}
	}
